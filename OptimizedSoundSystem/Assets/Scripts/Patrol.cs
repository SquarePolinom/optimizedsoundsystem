﻿using UnityEngine;

public class Patrol : MonoBehaviour
{

    [SerializeField]
    float amplitude = 4;
    [SerializeField]
    float speed = 5;

    float m_leftBorder, m_rightBorder;
    int m_direction;

    void Awake()
    {
        m_leftBorder = transform.localPosition.x - amplitude;
        m_rightBorder = transform.localPosition.x + amplitude;
        m_direction = 1;

    }

    void Update()
    {
        transform.Translate(Vector3.right * m_direction * speed * Time.deltaTime);

        if (transform.localPosition.x <= m_leftBorder || transform.position.x >= m_rightBorder)
        {
            m_direction = -m_direction;
        }
    }

}