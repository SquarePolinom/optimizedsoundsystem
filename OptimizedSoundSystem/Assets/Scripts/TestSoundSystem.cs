﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSoundSystem : MonoBehaviour
{
    [SerializeField]
    GameObject[] objToTest;

    float timer = 5f;
    bool isMade;

    void Start()
    {
        SoundManager.instance.PlaySound("Boom", is3Dsound:true,objectOf3DSound:objToTest[0], mustFollow:true);
        SoundManager.instance.PlaySound("GreenDaze", is3Dsound: true, objectOf3DSound: objToTest[1], mustFollow: true);        
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0 && !isMade)
        {
            isMade = true;
            SoundManager.instance.StopSound("Boom");
            SoundManager.instance.PlaySound("SportsAction", is3Dsound: true, objectOf3DSound: objToTest[0], mustFollow: true);
        }
    }
   
}
