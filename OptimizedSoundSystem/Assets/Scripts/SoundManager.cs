﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager instance = null;
    const float partOfSoundsToPull2D = 0.2f;
    const float partOfSoundsToPull3D = 0.2f;

    [SerializeField]
    List<AudioClip> allSounds = new List<AudioClip>();

    Queue<AudioSource> audioListenersPull = new Queue<AudioSource>();
    List<AudioSource> audioListenersInUse = new List<AudioSource>();

    Queue<AudioSource> audioListenersOnObjectsPull = new Queue<AudioSource>();
    List<AudioSource> audioListenersOnObjectsInUse = new List<AudioSource>();
    

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            FillAudioListersPull((int)(allSounds.Count * partOfSoundsToPull2D), (int)(allSounds.Count * partOfSoundsToPull3D));
        }
        else if (instance != this)
        {
            Destroy(this);
        }

        DontDestroyOnLoad(gameObject);
    }

    void FillAudioListersPull(int amountOfListners = 0, int amountOfObjects = 0)
    {
        for (var i = 0; i < amountOfListners; i++)
        {
            AudioSource newAudioSource = gameObject.AddComponent<AudioSource>();
            newAudioSource.enabled = false;
            audioListenersPull.Enqueue(newAudioSource);
        }

        for (var i = 0; i < amountOfObjects; i++)
        {
            AudioSource newObj = CreateObjectWithAudioListener();
            newObj.gameObject.SetActive(false);
            audioListenersOnObjectsPull.Enqueue(newObj);
        }
    }

    AudioSource CreateObjectWithAudioListener()
    {
        GameObject obj = new GameObject();
        return obj.AddComponent<AudioSource>();
    }


    public void PlaySound(string soundName, float volume = 1, bool isLooping = false, bool is3Dsound = false, GameObject objectOf3DSound = null, bool mustFollow = false)
    {
        if (is3Dsound && objectOf3DSound == null)
        {
            Debug.Log("No object for 3d sound");
        }
        else
        {
            AudioClip clip = allSounds.Find(x => x.name == soundName);
            if (clip != null)
            {
                if (is3Dsound)
                {
                    Play3DSound(clip, objectOf3DSound, mustFollow, isLooping, volume);
                }
                else
                {
                    Play2DSound(clip, isLooping, volume);
                }
            }
            else
            {
                Debug.Log("Unknown audio clip");
            }
        }
    }

    void Play3DSound(AudioClip clip, GameObject objectOf3DSound, bool mustFollow, bool isLooping, float volume)
    {
        AudioSource source;
        if (audioListenersOnObjectsPull.Count > 0)
        {
            source = audioListenersOnObjectsPull.Dequeue();
            source.gameObject.SetActive(true);
            
        }
        else
        {
            source = CreateObjectWithAudioListener();
        }        
        
        source.enabled = true;
        source.clip = clip;
        source.volume = volume;
        source.loop = isLooping;
        source.spatialBlend = 1;

        if (mustFollow)
        {
            source.transform.SetParent(objectOf3DSound.transform);
            source.transform.localPosition = Vector3.zero;
        }
        else
        {
            source.transform.position = objectOf3DSound.transform.position;
        }
        audioListenersOnObjectsInUse.Add(source);
        if (!isLooping)
        {
            StartCoroutine(RemoveClipFromList(source, source.clip.length, true));
        }
        source.Play();
    }

    void Play2DSound(AudioClip clip, bool isLooping, float volume)
    {
        AudioSource source;
        if (audioListenersPull.Count > 0)
        {
            source = audioListenersPull.Dequeue();
            source.enabled = true;
        }
        else
        {
            source = gameObject.AddComponent<AudioSource>();
        }
        
        source.clip = clip;
        source.volume = volume;
        source.loop = isLooping;
        
        audioListenersInUse.Add(source);
        if (!isLooping)
        {
            StartCoroutine(RemoveClipFromList(source, source.clip.length, false));
        }
        source.Play();
    }


    public void StopSound(string soundName)
    {
        AudioSource source = audioListenersInUse.Find(x => x.clip.name == soundName);
        if (source != null)
        {
            StopSound(source, false);
        }
        else
        {
            source = audioListenersOnObjectsInUse.Find(x => x.clip.name == soundName);
            if (source != null)
            {
                StopSound(source, true);
            }
            else
            {
                Debug.Log("Trying to stop unplaying sound");
            }
        }
    }

    void StopSound(AudioSource source, bool is3DSound)
    {
        source.Stop();
        source.enabled = false;
        if (is3DSound)
        {
            source.gameObject.SetActive(false);
            audioListenersOnObjectsInUse.Remove(source);
            audioListenersOnObjectsPull.Enqueue(source);
        }
        else
        {
            audioListenersInUse.Remove(source);
            audioListenersPull.Enqueue(source);
        }

    }

    IEnumerator RemoveClipFromList(AudioSource source, float time, bool is3Dsound)
    {
        yield return new WaitForSeconds(time);
        StopSound(source, is3Dsound);
    }
}
